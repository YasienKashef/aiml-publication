import { shallowMount } from "@vue/test-utils";
import TopicBanner from "@/components/TopicBanner.vue";

describe("TopicBanner.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "Rationals";
    const wrapper = shallowMount(TopicBanner, {
      propsData: { msg }
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
