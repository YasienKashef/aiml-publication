# Component Development

This guide explains how to develop a new type of component and integrate it in your data structure as well as on your website.  
Please follow these steps before creating or changing the source code of the website, as it can change or corrupt other person's data.

### Quick Links:

[Architecture & Process](#architecture-&-process)  
1. [Step 1: Define Component's data structure in JSON](#step-1-define-component's-data-structure-in-JSON)
2. [Step 2: Create Vue-Component](#step-2-create-vue-component)
3. [Step 3: Integrate Vue-Component in View](#step-3-integrate-vue-component-in-view)  

<br/>

## Architecture & Process
The architecture & process of how the JSON data gets mapped to a Vue-Component, is better visualized as written:
![Architecture & Process](assets/architecture.png)
> Each path for any JSON/Asset/Components starts from the topic's path, for example if you want to reference the `data.json` of the topic *Rationals* in `topics.json`, you add the following path: `"rationals/data.json"`, the `public/data`-prefix is omitted. 

<br/>

## Step 1: Define Component's data structure in JSON

For each new type of component there should be a distinct defined structure to be made in the JSON-files. Starting from the new type to the different data needed so that the component functions properly. The type as well as the needed data can differ from component to component, therefore we will cover one full example of a new component, so that it should be clear how to create new ones. In this full example we will create a new type of component for **Video**.   
  
Follow these steps to defined the new component's data structure in JSON:  
1. Go into any `data.json` of a subtopic. I.e. the subtopic which will use your new component.
2. In the `components`-array create the new object with it's attributes, for example:
~~~json
{
   "components": [
      {
         "type": "video",     // Set your type name
         "content": "topic_name/subtopic_name/components/videos/video.mp4",   // path to the video
         "center": true,      // Centers component if true
         "displayComponent": true,   // Displays component if true
         // Add additional data as you want, for e.g.
         "dimensions": {      // Dimensions of video
            "width": "",
            "height": ""
         },
         "autoplay": true,    // Autoplays video if true.
         // other data....
      },
      ...,
   ]
}
~~~
> Now you should have all the data needed and customizations for your component to be integrated in the website.

<br/>

## Step 2: Create Vue-Component
For each type of component there is a [Vue-Component](https://vuejs.org/v2/guide/single-file-components.html) responsible for handling the JSON data as well as creating and styling the HTML elements finally. You need to have some basic knowledge about [Vue.js](https://vuejs.org/), therefore we recommend that you check out their [Guide](https://vuejs.org/v2/guide/) as well as their [API](https://vuejs.org/v2/api/) for more information. For a quick crash course you need to know that, each `Component.vue` is constructed of three parts:
1. `<template>`-part: Is HTML-Code, here you will create all HTML elements of this component.
2. `<script>`-part: Is JavaScript Code in addition to Vue.js-Framework, here you will define the component and get the stored JSON data from the [Vue Store](https://vuex.vuejs.org/) , get access to the life-cycle hooks of the Vue App, as well as create logic and modifications to your component.
3. `<style>`-part: Is SCSS/CSS Code, here you will style your HTML elements of this component.
  
With this information, let us continue our example of creating a new **Video** component for our website. Follow these steps to create the new component in Vue:  
1. Go into `src/components/Subtopic/` in the source folder of the website. As you can see, here lay all used types of components in the website. If you get stuck, check out the other components too.
2. Copy the existing component `Graphic.vue` for a better start rather than creating a plain one. You can change everything still as you want, it's just better to have the three different parts mentioned above already laid out.
3. In the Vue-File, we will start with the `<script>`-Section:
   1. Change the `name` of the component to `Video`, as follows:
      ~~~vue
      <script>
      export default {
         name: "Video"
      };
      </script>
      ~~~
   2. Leave the `props`-Object as it is with the attribute `component`. The `component`-Object will hold all the data of the JSON component created in step 1. How we will achieve this, you will see in [Step 3]()
   3. In the end it shoud look something like this:
      ~~~vue
      <script>
      export default {
         name: "Video",
         pros: {
            component: Object
         }
      };
      </script>
      ~~~
   4. If you're experienced with Vue.js, you can add here specific methods or life-cycle hooks for this component:
      ~~~vue
      <script>
      export default {
         name: "Video",
         pros: {
            component: Object
         },
         methods: {
            method1() {
               // Call method1.
            }
         },
         mounted() {
            // Do Something when mounted.
         }
      };
      </script>
      ~~~
4. In the Vue-File, change the `<template>`-Section:
   1. An empty `<template>`-Section would look like this:
      ~~~vue
      <template>
      </template>
      ~~~
   2. Now create a HTML `video` element inside it, as follows:
      ~~~vue
      <template>
         <video />
      </template>
      ~~~
   3. As we said in the step above, that all JSON data already exists now in the Object `component`. We will use it to apply our data on this `video` element. For e.g. if we want to set the width and height of this video to the dimensions specified in the JSON component, we would do it as follows:
      ~~~vue
      <template>
         <video
            :width="this.component.dimensions.width"
            :height="this.component.dimensions.height"
         />
      </template>
      ~~~
      > For more information about this syntax see [Vue Template Syntax: v-bind](https://vuejs.org/v2/guide/syntax.html#Attributes)  
      >  `this.component` is **the** JSON-Object created in `data.json`, all the other selectors are exactly the same as the keys defined in your JSON-Object, for e.g. the `dimensions` are accessed through the same key `dimensions`.
   4. To add the source of the video (which is also specified in the JSON-Object in `data.json`) we would do it as follows:
      ~~~vue
      <template>
         <video
            :src="'data/' + this.component.content"
            :width="this.component.dimensions.width"
            :height="this.component.dimensions.height"
         />
      </template>
      ~~~
      >  Notice that we concatenated a string `'data/'` first to the actual path of the video set in the `data.json`. We do this so we can ommit the `data/`-string in the JSON-Object, where actually all paths start.
      4. To add the source of the video (which is also specified in the JSON-Object in `data.json`) we would do it as follows:
      ~~~vue
      <template>
         <video
            :src="'data/' + this.component.content"
            :width="this.component.dimensions.width"
            :height="this.component.dimensions.height"
         />
      </template>
      ~~~
      >  Notice that we concatenated a string `'data/'` first to the actual path of the video set in the `data.json`. We do this so we can ommit the `data/`-string in the JSON-Object, where actually all paths start.
   5. Finally you can add whatever you want in your HTML element or create new ones, for e.g. if you want to have a title below each video:
      ~~~vue
      <template>
         <video
            :src="'data/' + this.component.content"
            :width="this.component.dimensions.width"
            :height="this.component.dimensions.height"
            controls
            autplay
         />
         <span class="title">{{ this.component.videoTitle }}</span>
      </template>
      ~~~
      > For more information about this syntax see [Vue Template Syntax: Interpolations](https://vuejs.org/v2/guide/syntax.html#Interpolations)
5. In the Vue-File, change the `<style>`-Section:
   1. An empty `<style>`-Section would look like this:
      ~~~vue
      <style scoped lang="scss">
      </style>
      ~~~
      > `scoped` means that any styling made here will only effect the HTML elements inside this file, even if you use an element selector or a general selector.  
      > `lang="scss"` means the the styling is written in SCSS. If nothing is defined the styling is written in CSS. 
   2. Now create a `video`-selector inside it to style the video element, as follows:
      ~~~vue
      <style scoped lang="scss">
      video {
         color: blue;
      }
      </style>
      ~~~
   3. Or create a `.title`-class selector inside it to style the video title, as follows:
      ~~~vue
      <style scoped lang="scss">
      video {
         color: blue;
      }

      .title {
         font-size: 2rem;
         text-decoration: underline;
      }
      </style>
      ~~~
> Now you should have a ready-to-be-integrated Vue-component.

<br/>

## Step 3: Integrate Vue-Component in View
The final step is to integrate the newly created Vue-Component in your view. Without this step, you will have a correctly working Vue-Component, but it will not be displayed even if the `data.json` of the subtopic had this component in it.
The integration of a component is straight-forward, as follows:
1. Go into `src/views/` in the source folder of the website.
2. Open `Subtopic.vue`.
3. In the `<script>`-Section:
   1. Import your new component to the other components:
      ~~~js
      // Other imported components,
      import Video from "@/components/Subtopic/Video.vue";
      ~~~
   2. Register it as a component in the `components`-Object:
      ~~~js
      export default {
         name: "Subtopic",
         components: {
            // Other registered components,
            Video
         }
      }
      ~~~
3. In the `<template>`-Section:
   1. In the `<div>` with the class `"component"` create a Vue-Element for your Video as follows:
      ~~~vue
      <div
         class="component"
         v-for="(component, i) in subtopicComponents"
         :key="i"
      >
      <!-- Other imported Components -->
      <Video />
      ~~~
   2. Add the `v-if`-condition to it specifiying the same type set in the `data.json`:
      ~~~vue
      <div
         class="component"
         v-for="(component, i) in subtopicComponents"
         :key="i"
      >
      <!-- Other imported Components -->
      <Video v-if="component.type === 'video" />
      ~~~
   3. Pass the JSON-Object to the prop `component`, so that the data can be accessed in `Video.vue`, as follows:
      ~~~vue
      <div
         class="component"
         v-for="(component, i) in subtopicComponents"
         :key="i"
      >
      <!-- Other imported Components -->
      <Video v-if="component.type === 'video" :component="component" />
      ~~~
> You're now done ! Your new component is successfully integrated in the website and can be re-used in multiple subtopics !  
> Any change made in `Video.vue` will affect all videos on your website, therefore you dont need to bother yourself to change for e.g. the style of the videos individually for each video.
