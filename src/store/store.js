/**
 * The store.js is the state management of your spot, as a Vuex.Store.
 * @author Yasien Kashef
 * @version 1.0.0
 * @requires [Vue,Vuex]
 */
/* eslint-disable */
import Vue from "vue"; // to use the Vue.js-Framework.
import Vuex from "vuex"; // to use the Vuex.Store.
import router from "@/router/router";
import axios from "axios"; // to use Axios

Vue.use(Vuex); // to use the plugin Vuex.Store globally

/**
 * The Store which gets included in the main vue-application.
 * @since 1.0.0
 */
export default new Vuex.Store({
  // States
  state: {
    status: 'loading',
    currentTopic: -1,
    topics: [],
    currentSubtopic: ""
  },
  mutations: {
    setStatus(state, status) {
      state.status = status;
    },
    setCurrentTopic(state, index) {
      state.currentTopic = index;
      if(state.currentTopic >= 0) {
        router.push({
          path: "/Topic",
          query: {
            topic: index
          }
        })
        let topicLinks = document.getElementsByClassName("topic-link");
        for (var i = 0; i < topicLinks.length; i++) {
          if (i === index) {
            topicLinks[i].style.color = "rgb(66, 185, 131)";
          } else {
            topicLinks[i].style.color = "rgba(255, 255, 255, 0.7)";
          }
        }
      } else {
        document.getElementsByClassName("topic-link").forEach((link) => link.style.color = "rgba(255, 255, 255, 0.7)");
      }
    },
    /**
     * Sets the returned ascData.json from Grassfish in the state ascData.
     * @since 1.0.0
     * @param {this} state
     * @param {Object} payload : ascData.json
     */
    storeTopic(state, data) {
      state.topics.push(data);
    },
    storeCurrentSubtopic(state, data) {
      state.currentSubtopic = data;
    }
  },
  actions: {
    /**
     * Loads PlayerData to the playerData-state to access it.
     * If in development it sets a local data source which is located under '/public/'
     * If in production the playerData is stored at the set url for each player.
     * @since 1.1.0
     * @param {this} context
     * @returns {Promise} a promise which is successfull if playerData was set and loaded correctly, otherwise an error message.
     */
    loadTopics(context) {
      context.commit("setStatus", "loading")
      // This promise returns the masterDetails.json if resolved, otherwise rejects an error message.
      let url = "./data/topics.json";
      return axios
        .get(url)
        .then(async (response) => {
          for(const file of response.data) {  
            await this.dispatch("loadTopic", "data/"+file)
          }
        })
        .then(() => {
          context.commit("setStatus", "ready")
        })
        .catch((error) => {
          console.error(error);
        });
    },
    loadTopic(context, urlToTopic) {
      let url = urlToTopic;
      return axios
        .get(url)
        .then((response) => {
          context.commit("storeTopic", response.data);
        })
        .catch(function(error) {
          console.error(error);
        });
    },
    loadCurrentSubtopic(context, indexes) {
      let url = "data/"+this.state.topics[indexes.topicIndex].subtopics[indexes.subtopicIndex].components;
      return axios
        .get(url)
        .then((response) => {
          context.commit("storeCurrentSubtopic", response.data);
          console.log(response.data);
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }
});
