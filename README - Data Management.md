# Data Management

This guide explains how to manage the data of you website.  
Please follow these steps before creating, changing or managing the data of the website, as it can change or corrupt other person's data.

### Quick Links:

[Data Structure](#data-structure)
#### Main Topic  
1. [Create new Topic](#create-new-main-topic)  
2. [Change Content of a Main Topic](#change-content-of-a-main-topic)
#### Subtopic
1. [Change Content of a Subtopic Preview](#change-content-of-a-subtopic-preview) 
2. [Create new Subtopic](#create-new-subtopic)
3. [Create new Subtopic Components](#create-new-subtopic-components)
#### Components
1. [Abstract](#abstract)
2. [Paragraph](#paragraph)
3. [Image/GIF](#image/gif)
4. [Video](#video)
5. [Custom](#custom)



## Data Structure
The data structure of the website is build as follows:
```  
`public/data`  
├─── `topics.json`  
│   ├─── path to main topic 1   
│   ├─── path to main topic 2  
│   ├─── path to main topic ...  
│   └─── path to main topic n  
│   
└─── `maintopic-folder`  
    ├─── `data.json`  
    │   ├─── Main Topic data, i.e. Title, Subtitle, Thumbnail..  
    │   ├─── Subtopic Preview 1  
    │   │    └─── path to Subtopic 1 
    │   └─── Subtopic Preview 2  
    │        └─── path to Subtopic 2  
    │  
    └─── `subtopics-folders`   
        ├─── `data.json`  
        │    └─── components of subtopic  
        │  
        ├─── `components-folder`  
        └─── `assets-folder`
```
> Each path for any JSON/Asset/Components starts from the topic's path, for example if you want to reference the `data.json` of the topic *Rationals* in `topics.json`, you add the following path: `"rationals/data.json"`, the `public/data`-prefix is omitted. 

<br/>

## Create new Main Topic
**These steps are for creating a new *Main* Topic, if you only want to create a new subtopic in an already existing main topic, see [Create new Subtopic](#create-new-subtopic)**  

Main Topics are topics displayed on the Home View with a preview component and displayed in the topbar of the website. A Main Topic consists of data describing itself as well as holding the path's and preview data for it's subtopics. Each Main Topic has it's own dynamically created view, displaying it's content listed in the corresponding `data.json`.   
  
Follow these steps to create a new topic:  
1. Copy any existing topic-folder in `public/data`.
2. Rename the folder to the topic's name you want.
3. Open `topics.json` in `public/data` and add the path of your new topic in the array, as follows:
~~~json
[
   ...,
   "new_topic/data.json"
]
~~~
> Refresh the webpage, you have now created a new topic !  
You will see it appear in the topbar and on the homeview automatically. But you surely want to change the copied content, see below for more information.

<br/>

## Change Content of a Main Topic
Follow these steps to change the content of a main topic:  
1. Open `data.json` in the maintopic-folder of the main topic you want to change.
2. Change the topic's name:
~~~json
{
   "topicName": "My new Topic",
   ...
}
~~~
3. Change the subtitle:
~~~json
{
   ...,
   "subtitle": "This topic is ...",
   ...
}
~~~
4. Change the description:
~~~json
{
   ...,
   "description": "This topic is about ...",
   ...
}
~~~
5. Change the thumbnail:
~~~json
{
   ...,
   "thumbnail": {
       "path": "path to my thumbnail starting from topic's folder.",     // e.g. "topic_name/assets/thumbnail.png"
       "dimensions": {
           "width": "50%",  // All CSS dimensions are allowed.
           "height": ""
       }
   },
   ...
}
~~~
6. Flag to hide/display the topic, without deleting it:
~~~json
{
   ...,
   "displayTopic": true    // The topic will be visible on the website.
}
~~~
> Refresh the webpage, you have now changed the content of this topic !  
You will see the changes in the topbar, the homeview as well as the view of the topics itself automatically. But you surely want to change and the copied subtopic previews, see below for more information.

<br/>

## Change Content of a Subtopic Preview
Subtopic Previews are displayed on the corresponding Topic View. They display some preview data about the subtopic and contain the path to the `data.json` of the subtopic.  

Follow these steps to change the content of a subtopic preview:  
1. Open `data.json` in the maintopic-folder of the main topic containing the subtopic.
2. Change the subtopic's preview name:
~~~json
{
   "topicName": "My new Subtopic",
   ...
}
~~~
3. Change the subtitle:
~~~json
{
   ...,
   "subtitle": "This subtopic is ...",
   ...
}
~~~
4. Change the thumbnail:
~~~json
{
   ...,
   "thumbnail": {
       "path": "path to my thumbnail starting from topic's folder.",     // e.g. "topic_name/subtopic_name/assets/thumbnail.png"
       "dimensions": {
           "width": "50%",  // All CSS dimensions are allowed.
           "height": ""
       }
   },
   ...
}
~~~
5. Flag to hide/display the subtopic, without deleting it:
~~~json
{
   ...,
   "displayTopic": true,    // The topic will be visible on the website.
   ...
}
~~~
6. Path to the subtopic's `data.json`:
~~~json
{
   ...,
   "components": "topic_name/subtopic_name/data.json"
}
~~~
> Refresh the webpage, you have now changed the content of this subtopic preview !  
You will see the changes in the Topic View containing this subtopic automatically. But you surely want to create the components of the Subtopic too ! See below for more information.

## Create new Subtopic
**These steps are for creating a new *Sub*topic, if you want to create a new main topic, see [Create new Main Topic](#create-new-main-topic)**

Subtopics are topics displayed on the Topic View with a preview component and its in own view. A Subtopic consists of an array of components creating itself. Each Subtopic has it's own dynamically created view, displaying it's content listed in the corresponding `data.json`.

Follow these steps to create a new topic:  
1. Copy any existing subtopic-folder in `public/data/topic_name`.
2. Rename the folder to the subtopic's name you want.
3. Open `data.json` of the Main Topic containing the new subtopic, in `public/data/topic_name`, and create a new [Subtopic Preview](#change-content-of-a-subtopic-preview) in the existing `"subtopics"`-array, for e.g.:
~~~json
{
    ...,
    "subtopics": [
      {
         "topicName": "Existing Subtopic X",
         "subtitle": "Subtitle of existing Subtopic X",
         "thumbnail": {
            "path": "topic_name/existing_subtopic_x/thumbnail.svg",
            "dimensions": {
               "width": "",
               "height": "300px"
            }
         },
         "displayTopic": true,
         "components": "topic_name/existing_subtopic_x/data.json"
      }
    ]
}
~~~
4. Will be looking after adding the new Subtopic Preview:
~~~json
{
    ...,
    "subtopics": [
      {
         "topicName": "Existing Subtopic X",
         "subtitle": "Subtitle of existing Subtopic X",
         "thumbnail": {
            "path": "topic_name/existing_subtopic_x/thumbnail.svg",
            "dimensions": {
               "width": "",
               "height": "300px"
            }
         },
         "displayTopic": true,
         "components": "topic_name/existing_subtopic_x/data.json"
      },
      {
         "topicName": "New Subtopic Y",
         "subtitle": "Subtitle of new Subtopic Y",
         "thumbnail": {
            "path": "topic_name/new_subtopic_y/thumbnail.svg",
            "dimensions": {
               "width": "",
               "height": "300px"
            }
         },
         "displayTopic": true,
         "components": "topic_name/new_subtopic_y/data.json"
      }
    ]
}
~~~
> You need to create a Subtopic Preview for your Subtopic, otherwise your Subtopic will not be accessible through your Topic View !

> Refresh the webpage, you have now created a new subtopic !  
You will see it appear in the Topic View in the lower Subtopics and by clicking on the Subtopic Preview of it you will be redirected to the Subtopic View automatically. But you surely want to change the copied content of the subtopic, see below for more information.

<br/>

## Change Content of a Subtopic
Follow these steps to change the content of a subtopic:  
1. Open `data.json` in the subtopic-folder of the subtopic you want to change.
2. Change the topic's name:
~~~json
{
   "topicName": "My new Subtopic",
   ...
}
~~~
3. Change and Create new Subtopic Components as you wish !
> Refresh the webpage, you have now changed the content of this subtopic !  
You will see the changes in the Subtopic View automatically.

<br/>

## Create new Subtopic Components
Subtopic Components are the content of your subtopic, defined through the component type. There are pre-defined components as Paragraph or Image, but you can always create custom HTML components and use them, as seen below.

## Pre-defined Components
---------------------------

### Abstract
The abstract component displays a paragraph or text stored in the `data.json` of the subtopic, with a slightly bigger font size.   
E.g.:
~~~json
"components": [
    {
        "type": "abstract",
        "content": "Latest insights from biology show that intelligence does not only emerge from the connections between the neurons, but that individual neurons shoulder more computational responsibility. Current Neural Network architecture design and search are biased on fixed activation functions. Using more advanced learnable activation functions provide Neural Networks with higher learning capacity.",
        "displayComponent": true    // If false the component is hidden.
    },
    ...,
]
~~~

<br/>

### Paragraph
The paragraph component displays a paragraph or text stored in the `data.json` of the subtopic.   
E.g.:
~~~json
"components": [
    {
        "type": "paragraph",
        "content": "Latest insights from biology show that intelligence does not only emerge from the connections between the neurons, but that individual neurons shoulder more computational responsibility. Current Neural Network architecture design and search are biased on fixed activation functions. Using more advanced learnable activation functions provide Neural Networks with higher learning capacity.",
        "displayComponent": true    // If false the component is hidden.
    },
    ...,
]
~~~

<br/>

### Image/GIF
The Image/GIF component displays a graphic, image or GIF with the common used formats in web (png, jpg, svg, gif, etc.) stored in the subtopic's folder.   
E.g.:
~~~json
"components": [
      {
         "type": "image/gif",
         "content": "topic_name/subtopic_name/the_path_to_your_picture.png",
         "dimensions": {
            "width": "50%",     // All CSS dimensions are allowed.
            "height": ""
         },
         "center": true,    // If true it's centered in the website.
         "displayComponent": true   // If false the component is hidden.
      },
    ...,
]
~~~

<br/>

### Video
The Video component displays a video with the common used formats in web (mp4, mov, webm, etc.) stored in the subtopic's folder.   
E.g.:
~~~json
"components": [
      {
         "type": "video",
         "content": "topic_name/subtopic_name/the_path_to_your_video.mp4",
         "dimensions": {
            "width": "50%",     // All CSS dimensions are allowed.
            "height": ""
         },
         "center": true,    // If true it's centered in the website.
         "displayComponent": true   // If false the component is hidden.
      },
    ...,
]
~~~

### Custom
The custom component is made for any other HTML components, wrapped in an `index.html`-file. You can display anything web-based through this component as visualizations, HTML-Plots, galleries and more. The source-files are stored in the subtopic's folder.   
E.g.:
~~~json
"components": [
      {
         "type": "custom",
         "content": "topic_name/subtopic_name/the_path_to_your_index.html",
         "dimensions": {
            "width": "600px",     // All CSS dimensions are allowed.
            "height": "400px"
         },
         "center": true,    // If true it's centered in the website.
         "displayComponent": true   // If false the component is hidden.
      },
    ...,
]
~~~